# code to prepare `so_ii_hydro` dataset goes here

selection = c("CdOH", "TopoOH")
file_dir = so.ii::current_version(
    "data-common/so-ii/topage",
    pattern = "^[0-9-]+$"
)
river = sf::st_read(file.path(file_dir, "cours_eau.shp"))
river = sf::st_transform(
    river[selection],
    sf::st_crs(so.ii::so_ii_limit)
)
names(river) = c("id", "name", "geometry")

classification = read.csv2(
    so.ii::current_version("data-common/so-ii/topage", pattern = "cours_eau"),
    colClasses = "character",
    row.names = 1
)
river = merge(river, classification)

waterbody = sf::st_read(file.path(file_dir, "plan_eau.shp"))
waterbody[["name"]] = gsub("C[?]ur", "Cœur", waterbody[["name"]])

classification = read.csv2(
    so.ii::current_version("data-common/so-ii/topage", pattern = "plan_eau"),
    colClasses = "character"
)
waterbody = merge(waterbody, classification)

so_ii_hydro = rbind(river, waterbody)

so_ii_hydro[["degre"]] = factor(so_ii_hydro[["degre"]])
so_ii_hydro[["type"]] = factor(so_ii_hydro[["type"]])
Encoding(waterbody[["name"]]) = "UTF-8"

# updating datasets

actual = setwd("so.ii")
usethis::use_data(so_ii_hydro, internal = FALSE, overwrite = TRUE)
setwd(actual)
