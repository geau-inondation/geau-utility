# code to prepare `so_ii_inset` dataset goes here

file_dir = so.ii::current_version("data-common/data/IGN/ADMIN-EXPRESS/version")

france = sf::st_read(file.path(file_dir, "DEPARTEMENT.shp"))
selection = france[["INSEE_DEP"]] < "96" &
            france[["INSEE_DEP"]] != "2A" & france[["INSEE_DEP"]] != "2B"
france = france[selection, ]
france = rmapshaper::ms_simplify(france, keep = 0.01)
departement =  sf::st_sf(
    "scope" = "Hérault",
    geometry = sf::st_geometry(france[france[["INSEE_DEP"]] == "34", ])
)
region = sf::st_sf(
    "scope" = "Occitanie",
    geometry = sf::st_union(france[france[["INSEE_REG"]] == "76", "geometry"])
)
france = sf::st_sf(
    "scope" = "France",
    geometry = sf::st_union(france)
)
so.ii = sf::st_sf(
    "scope" = "so-ii",
    geometry = sf::st_geometry(rmapshaper::ms_simplify(so.ii::so_ii_limit, keep = 0.01))
)
so_ii_inset = rbind(france, region, departement, so.ii)
rownames(so_ii_inset) = c("nation", "region", "department", "so-ii")
so_ii_inset = sf::st_transform(so_ii_inset, crs = sf::st_crs(so.ii::so_ii_limit))

# updating datasets

actual = setwd("so.ii")
usethis::use_data(so_ii_inset, internal = FALSE, overwrite = TRUE)
setwd(actual)




