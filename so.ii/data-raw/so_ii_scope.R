# code to prepare `so_ii_scope` dataset goes here

so_ii_scope = read.csv2(
    current_version("data-common/so-ii/scope"),
    colClasses = "character"
)[["code"]]
so_ii_scope = sort(so_ii_scope)

# updating datasets

actual = setwd("so.ii")
usethis::use_data(so_ii_scope, internal = FALSE, overwrite = TRUE)
setwd(actual)
