
```{r data-recup}
# Récupération des données (à faire une fois)

## Récupération du périmètre (déjà pré-traité)
soii = readRDS("data-common/data/so-ii/so-ii_perim.rds")

## Récupération du périmètre dans France (déjà prétraité)
soii_france = readRDS("data-common/data/so-ii/so-ii_france.rds")

## Récupération de CLC (déjà pré-traité)
soii_clc = readRDS("data-common/data/so-ii/so-ii_clc.rds")

## Recupération des données du fond de carte
# soii_osm = maptiles::get_tiles(soii, provider = "OpenStreetMap.MapnikBW", crop = TRUE, zoom = 11)
# terra::writeRaster(soii_osm, "data-common/data/so-ii/so-ii_osm-bw.tif", overwrite = TRUE)
# soii_osm = terra::rast("data-common/data/so-ii/so-ii_osm-bw.tif")
# credit = sprintf("Background from %s", maptiles::get_credit("OpenStreetMap"))
```

```{r rex_2020}
# Production d'une carte

## Définition du nom du fichier de sauvegarde
file_path = file.path("data-common/figure/so-ii/pdf/2021-07-20/rex_2020.pdf")

## Récupération des données propre à la carte en cours
rex_2020 = rio::import("data-common/table/so-ii/rex-2020.ods", which = 1)
## Transformation des données d'enquête en points géoréférencés
rex_2020 = sf::st_as_sf(rex_2020, coords = c("longitude", "latitude"), crs = "WGS84")

# Génération de la carte

## Ouverture du fichier de sauvegarde
# png(file_path, height = 800, width = 800)
pdf(file_path)

## Initialisation de la carte avec le périmètre de so-ii
par(mai = c(.65, .60, .50, .15))
plot(soii, axes = TRUE)
# terra::plotRGB(soii_osm, add = TRUE)
plot(soii, lwd = 2, add = TRUE)

## Ajout du fond CLC avec le niveau 1 du code_18
color_clc = scales::alpha(c("red3", "darkolivegreen3", "darkgreen", "#4C90B4", "lightblue"), .2)
color = as.character(cut(
    as.integer(substr(soii_clc[["code_18"]], 1, 1)), 
    breaks = 5,
    labels = color_clc))
plot(soii_clc$geometry, border = NA, col = color, add = TRUE)

## Ajout des points d'enquête
# plot(rex_2020$geometry, col = "red", bg = "red", pch = 21, cex = 1, add = TRUE)
plot(rex_2020$geometry[rex[["viticulture"]],],
    col = "black", bg = "deeppink4", cex = 1.4,
    pch = 21, add = TRUE)
plot(rex_2020$geometry[rex[["habitant"]],],
    col = "black", bg = "cornflowerblue", cex = 1.4,
    pch = 21, add = TRUE)
# plot(rex_2020$geometry[rex[["maraichage"]],],
    # col = "black", bg = "green", cex = 1.4,
    # pch = 21, add = TRUE)

## Ajout de l'échelle
# raster::scalebar(10, type = "bar", divs = 4, below = "km")
terra::sbar(10, c(3.55, 43.47), type = "bar", below = "km", label = c(0, 5, 10), cex = .8)

## Ajout des crédits
# text(3.55, 43.435, credit, adj = 0, cex = .8, font = 3)

## Légende

# legend("topright", cex = .8, bg = "white", inset = 0.01,
    # title = "Land Use (from CLC)",
    # legend = c(
        # "Urban area",
        # "Agricultural area",
        # "Forest, natural area",
        # "Humid area",
        # "Water surface"),
    # fill = color_clc)

legend("bottomright", cex = .8, bg = "white", inset = 0.01,
    title = "Enquêtes du REX 19 septembre 2020 :",
    legend = c("Agriculteurs", "Habitants membre ROI"),
    pch = 21,
    col = "black",
    pt.bg = c("deeppink4", "cornflowerblue"),
    pt.cex = 1.4)

## Ajout de l'encart
# geau::add.inset(soii_france, col = c("white", "red"), border = c("lightgray", "red"), lwd = 2)

## Fermeture du fichier de sauvegarde
dev.off()  
```

```{r roi_agri}
# Production d'une carte

## Définition du nom du fichier de sauvegarde
file_path = file.path("data-common/figure/so-ii/pdf/2021-07-20/roi_agri.pdf")

## Récupération des données propre à la carte en cours
roi_agri = rio::import("data-common/table/so-ii/roi-agri.ods", which = 1)
## Transformation des données d'enquête en points géoréférencés
roi_agri = sf::st_as_sf(rex_2020, coords = c("longitude", "latitude"), crs = "WGS84")

# Génération de la carte

## Ouverture du fichier de sauvegarde
# png(file_path, height = 800, width = 800)
pdf(file_path)

## Initialisation de la carte avec le périmètre de so-ii
par(mai = c(.65, .60, .50, .15))
plot(soii, axes = TRUE)
# terra::plotRGB(soii_osm, add = TRUE)
plot(soii, lwd = 2, add = TRUE)

## Ajout du fond CLC avec le niveau 1 du code_18
color_clc = scales::alpha(c("red3", "darkolivegreen3", "darkgreen", "#4C90B4", "lightblue"), .2)
color = as.character(cut(
    as.integer(substr(soii_clc[["code_18"]], 1, 1)), 
    breaks = 5,
    labels = color_clc))
plot(soii_clc$geometry, border = NA, col = color, add = TRUE)

## Ajout des points d'enquête
# plot(roi_agri$geometry, col = "red", bg = "red", pch = 21, cex = 1, add = TRUE)
plot(roi_agri$geometry[roi_agri[["activite"]] == "viticulture"],
    col = "black", bg = "gray", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_agri$geometry[roi_agri[["activite"]] == "maraichage"],
    col = "black", bg = "green", cex = 1.4,
    pch = 21, add = TRUE)
## Ajout de l'échelle
# raster::scalebar(10, type = "bar", divs = 4, below = "km")
terra::sbar(10, c(3.55, 43.47), type = "bar", below = "km", label = c(0, 5, 10), cex = .8)

## Ajout des crédits
# text(3.55, 43.435, credit, adj = 0, cex = .8, font = 3)

## Légende

# legend("topright", cex = .8, bg = "white", inset = 0.01,
    # title = "Land Use (from CLC)",
    # legend = c(
        # "Urban area",
        # "Agricultural area",
        # "Forest, natural area",
        # "Humid area",
        # "Water surface"),
    # fill = color_clc)

legend("bottomright", cex = .8, bg = "white", inset = 0.01,
    title = "Agriculteurs contactées/à contacter:",
    legend = c("Agriculteurs"),
    pch = 21,
    col = "black",
    pt.bg = c("gray"),
    pt.cex = 1.4)

## Ajout de l'encart
# geau::add.inset(soii_france, col = c("white", "red"), border = c("lightgray", "red"), lwd = 2)

## Fermeture du fichier de sauvegarde
dev.off()  
```

```{r presence_candidats}
# Production d'une carte

## Définition du nom du fichier de sauvegarde
file_path = file.path("data-common/figure/so-ii/pdf/2021-07-20/roi_candidat_presence.pdf")

## Récupération des données propre à la carte en cours
roi_habitant = rio::import("data-common/table/so-ii/roi-habitant-2021-07-20.ods", which = 1)
roi_habitant = roi_habitant[!is.na(roi_habitant$latitude), ]

## Transformation des données d'enquête en points géoréférencés
roi_habitant = sf::st_as_sf(roi_habitant, coords = c("longitude", "latitude"), crs = "WGS84")

# Génération de la carte

## Ouverture du fichier de sauvegarde
# png(file_path, height = 800, width = 800)
pdf(file_path)

## Initialisation de la carte avec le périmètre de so-ii
par(mai = c(.65, .60, .50, .15))
plot(soii, axes = TRUE)
# terra::plotRGB(soii_osm, add = TRUE)
plot(soii, lwd = 2, add = TRUE)

## Ajout du fond CLC avec le niveau 1 du code_18
color_clc = scales::alpha(c("red3", "darkolivegreen3", "darkgreen", "#4C90B4", "lightblue"), .2)
color = as.character(cut(
    as.integer(substr(soii_clc[["code_18"]], 1, 1)), 
    breaks = 5,
    labels = color_clc))
plot(soii_clc$geometry, border = NA, col = color, add = TRUE)

## Ajout des points d'enquête
# plot(candidats$geometry, col = "red", bg = "red", pch = 21, cex = 1, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["statut_personne"]] == "abandon"],
    col = "black", bg = "gray55", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["statut_personne"]] == "non"],
    col = "black", bg = "gray55", cex = 1.4,
    pch = 21, add = TRUE) 
plot(roi_habitant$geometry[
        roi_habitant[["statut_personne"]] == "candidat" & 
        roi_habitant[["presence_atelier"]] == "non"
    ],
    col = "black", bg = "lightskyblue", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[
        roi_habitant[["statut_personne"]] == "candidat" & 
        roi_habitant[["presence_atelier"]] == "oui"
    ],
    col = "black", bg = "limegreen", cex = 1.4,
    pch = 21, add = TRUE)
   
## Ajout de l'échelle
# raster::scalebar(10, type = "bar", divs = 4, below = "km")
terra::sbar(10, c(3.55, 43.47), type = "bar", below = "km", label = c(0, 5, 10), cex = .8)

## Ajout des crédits
# text(3.55, 43.435, credit, adj = 0, cex = .8, font = 3)

## Légende

# legend("topright", cex = .8, bg = "white", inset = 0.01,
    # title = "Land Use (from CLC)",
        # "Urban area",
        # "Agricul
```{r rex_2020}
# Production d'une carte

## Définition du nom du fichier de sauvegarde
file_path = file.path("data-common/figure/so-ii/pdf/2021-07-20/rex_2020.pdf")

## Récupération des données propre à la carte en cours
rex_2020 = rio::import("data-common/table/so-ii/rex-2020.ods", which = 1)
## Transformation des données d'enquête en points géoréférencés
rex_2020 = sf::st_as_sf(rex_2020, coords = c("longitude", "latitude"), crs = "WGS84")

# Génération de la carte

## Ouverture du fichier de sauvegarde
# png(file_path, height = 800, width = 800)
pdf(file_path)

## Initialisation de la carte avec le périmètre de so-ii
par(mai = c(.65, .60, .50, .15))
plot(soii, axes = TRUE)
# terra::plotRGB(soii_osm, add = TRUE)
plot(soii, lwd = 2, add = TRUE)

## Ajout du fond CLC avec le niveau 1 du code_18
color_clc = scales::alpha(c("red3", "darkolivegreen3", "darkgreen", "#4C90B4", "lightblue"), .2)
color = as.character(cut(
    as.integer(substr(soii_clc[["code_18"]], 1, 1)), 
    breaks = 5,
    labels = color_clc))
plot(soii_clc$geometry, border = NA, col = color, add = TRUE)

## Ajout des points d'enquête
# plot(rex_2020$geometry, col = "red", bg = "red", pch = 21, cex = 1, add = TRUE)
plot(rex_2020$geometry[rex[["viticulture"]],],
    col = "black", bg = "deeppink4", cex = 1.4,
    pch = 21, add = TRUE)
plot(rex_2020$geometry[rex[["habitant"]],],
    col = "black", bg = "cornflowerblue", cex = 1.4,
    pch = 21, add = TRUE)
# plot(rex_2020$geometry[rex[["maraichage"]],],
    # col = "black", bg = "green", cex = 1.4,
    # pch = 21, add = TRUE)
deeppink4
## Ajout de l'échelle
# raster::scalebar(10, type = "bar", divs = 4, below = "km")
terra::sbar(10, c(3.55, 43.47), type = "bar", below = "km", label = c(0, 5, 10), cex = .8)

## Ajout des crédits
# text(3.55, 43.435, credit, adj = 0, cex = .8, font = 3)

## Légende

# legend("topright", cex = .8, bg = "white", inset = 0.01,
    # title = "Land Use (from CLC)",
    # legend = c(
        # "Urban area",
        # "Agricultural area",
        # "Forest, natural area",
        # "Humid area",
        # "Water surface"),
    # fill = color_clc)

legend("bottomright", cex = .8, bg = "white", inset = 0.01,
    title = "Enquêtes du REX 19 septembre 2020 :",
    legend = c("Agriculteurs", "Habitants membre ROI"),
    pch = 21,
    col = "black",
    pt.bg = c("deeppink4", "cornflowerblue"),
    pt.cex = 1.4)

## Ajout de l'encart
# geau::add.inset(soii_france, col = c("white", "red"), border = c("lightgray", "red"), lwd = 2)

## Fermeture du fichier de sauvegarde
dev.off()  
```


```{r presence_candidats}
# Production d'une carte

## Définition du nom du fichier de sauvegarde
file_path = file.path("data-common/figure/so-ii/pdf/2021-07-20/roi_candidat_presence.pdf")

## Récupération des données propre à la carte en cours
roi_habitant = rio::import("data-common/table/so-ii/roi-habitant-2021-07-20.ods", which = 1)
roi_habitant = roi_habitant[!is.na(roi_habitant$latitude), ]

## Transformation des données d'enquête en points géoréférencés
roi_habitant = sf::st_as_sf(roi_habitant, coords = c("longitude", "latitude"), crs = "WGS84")

# Génération de la carte

## Ouverture du fichier de sauvegarde
# png(file_path, height = 800, width = 800)
pdf(file_path)

## Initialisation de la carte avec le périmètre de so-ii
par(mai = c(.65, .60, .50, .15))
plot(soii, axes = TRUE)
# terra::plotRGB(soii_osm, add = TRUE)
plot(soii, lwd = 2, add = TRUE)

## Ajout du fond CLC avec le niveau 1 du code_18
color_clc = scales::alpha(c("red3", "darkolivegreen3", "darkgreen", "#4C90B4", "lightblue"), .2)
color = as.character(cut(
    as.integer(substr(soii_clc[["code_18"]], 1, 1)), 
    breaks = 5,
    labels = color_clc))
plot(soii_clc$geometry, border = NA, col = color, add = TRUE)

## Ajout des points d'enquête
# plot(candidats$geometry, col = "red", bg = "red", pch = 21, cex = 1, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["statut_personne"]] == "abandon"],
    col = "black", bg = "gray55", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["statut_personne"]] == "non"],
    col = "black", bg = "gray55", cex = 1.4,
    pch = 21, add = TRUE) 
plot(roi_habitant$geometry[
        roi_habitant[["statut_personne"]] == "candidat" & 
        roi_habitant[["presence_atelier"]] == "non"
    ],
    col = "black", bg = "lightskyblue", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[
        roi_habitant[["statut_personne"]] == "candidat" & 
        roi_habitant[["presence_atelier"]] == "oui"
    ],
    col = "black", bg = "limegreen", cex = 1.4,
    pch = 21, add = TRUE)
   
## Ajout de l'échelle
# raster::scalebar(10, type = "bar", divs = 4, below = "km")
terra::sbar(10, c(3.55, 43.47), type = "bar", below = "km", label = c(0, 5, 10), cex = .8)

## Ajout des crédits
# text(3.55, 43.435, credit, adj = 0, cex = .8, font = 3)

## Légende

# legend("topright", cex = .8, bg = "white", inset = 0.01,
    # title = "Land Use (from CLC)",
        # "Urban area",
        # "Agricultural area",
        # "Forest, natural area",
        # "Humid area",
        # "Water surface"),
    # fill = color_clc)

legend("bottomright", cex = .8, bg = "white", inset = 0.01,
    title = "Candidats du ROI Habitant :",
    legend = c("abandon / non retenu", "candidat non présent", "candidat présent"),
    pch = 21,
    col = "black",
    pt.bg = c("gray55", "lightskyblue", "limegreen"),
    pt.cex = 1.4)

## Ajout de l'encart
# geau::add.inset(soii_france, col = c("white", "red"), border = c("lightgray", "red"), lwd = 2)

## Fermeture du fichier de sauvegarde
dev.off()  
```

```{r type_inondation}
# Production d'une carte

## Définition du nom du fichier de sauvegarde
file_path = file.path("data-common/figure/so-ii/pdf/2021-07-20/type_inondation.pdf")

## Récupération des données propre à la carte en cours
roi_habitant = rio::import("data-common/table/so-ii/roi-habitant-2021-07-20.ods", which = 1)
roi_habitant = roi_habitant[!is.na(roi_habitant$latitude), ]

## Transformation des données d'enquête en points géoréférencés
roi_habitant = sf::st_as_sf(roi_habitant, coords = c("longitude", "latitude"), crs = "WGS84")

# Génération de la carte

## Ouverture du fichier de sauvegarde
# png(file_path, height = 800, width = 800)
pdf(file_path)

## Initialisation de la carte avec le périmètre de so-ii
par(mai = c(.65, .60, .50, .15))
plot(soii, axes = TRUE)
# terra::plotRGB(soii_osm, add = TRUE)
plot(soii, lwd = 2, add = TRUE)

## Ajout du fond CLC avec le niveau 1 du code_18
color_clc = scales::alpha(c("red3", "darkolivegreen3", "darkgreen", "#4C90B4", "lightblue"), .2)
color = as.character(cut(
    as.integer(substr(soii_clc[["code_18"]], 1, 1)), 
    breaks = 5,
    labels = color_clc))
plot(soii_clc$geometry, border = NA, col = color, add = TRUE)

## Ajout des points d'enquête
# plot(roi_habitant$geometry, col = "red", bg = "red", pch = 21, cex = 1, add = TRUE)
plot(roi_habitant$geometry[
        roi_habitant[["type_alea"]] == "debordement-cours-eau"] &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "blue", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["type_alea"]] == "debordement-cours-eau_ruissellement" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "lightblue", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["type_alea"]] == "debordement-etang" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "hotpink1", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["type_alea"]] == "remontee-nappe" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "green", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["type_alea"]] == "ruissellement" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "brown", cex = 1.4,
    pch = 21, add = TRUE)    
plot(roi_habitant$geometry[roi_habitant[["type_alea"]] == "rupture-digue"],
    col = "black", bg = "orange", cex = 1.4,
    pch = 21, add = TRUE)

## Ajout de l'échelle
# raster::scalebar(10, type = "bar", divs = 4, below = "km")
terra::sbar(10, c(3.55, 43.47), type = "bar", below = "km", label = c(0, 5, 10), cex = .8)

## Ajout des crédits
# text(3.55, 43.435, credit, adj = 0, cex = .8, font = 3)

## Légende
# legend("topright", cex = .8, bg = "white", inset = 0.01,
    # title = "Land Use (from CLC)",
    # legend = c(
        # "Urban area",
        # "Agricultural area",
        # "Forest, natural area",
        # "Humid area",
        # "Water surface"),
    # fill = color_clc)

legend("bottomright", cex = .8, bg = "white", inset = 0.01,
    title = "Exposition principale des candidats :",
    legend = c("débordement de cours d'eau", "débordement et ruissellement", "débordement d'étang", "remontée de nappe", "ruissellement", "rupture de digue"),
    pch = 21,
    col = "black",
    pt.bg = c("blue", "lightblue", "hotpink1", "green", "brown", "orange"),
    pt.cex = 1.4)

## Ajout de l'encart
# geau::add.inset(soii_france, col = c("white", "red"), border = c("lightgray", "red"), lwd = 2)

## Fermeture du fichier de sauvegarde
dev.off()  
```


```{r origine}
# Production d'une carte

## Définition du nom du fichier de sauvegarde
file_path = file.path("data-common/figure/so-ii/pdf/2021-07-20/origine.pdf")

## Récupération des données propre à la carte en cours
roi_habitant = rio::import("data-common/table/so-ii/roi-habitant-2021-07-20.ods", which = 1)
roi_habitant = roi_habitant[!is.na(roi_habitant$latitude), ]

## Transformation des données d'enquête en points géoréférencés
roi_habitant = sf::st_as_sf(roi_habitant, coords = c("longitude", "latitude"), crs = "WGS84")

# Génération de la carte

## Ouverture du fichier de sauvegarde
# png(file_path, height = 800, width = 800)
pdf(file_path)

## Initialisation de la carte avec le périmètre de so-ii
par(mai = c(.65, .60, .50, .15))
plot(soii, axes = TRUE)
# terra::plotRGB(soii_osm, add = TRUE)
plot(soii, lwd = 2, add = TRUE)

## Ajout du fond CLC avec le niveau 1 du code_18
color_clc = scales::alpha(c("red3", "darkolivegreen3", "darkgreen", "#4C90B4", "lightblue"), .2)
color = as.character(cut(
    as.integer(substr(soii_clc[["code_18"]], 1, 1)), 
    breaks = 5,
    labels = color_clc))
plot(soii_clc$geometry, border = NA, col = color, add = TRUE)

## Ajout des points d'enquête
# plot(roi_habitant$geometry, col = "red", bg = "red", pch = 21, cex = 1, add = TRUE)
plot(roi_habitant$geometry[
        roi_habitant[["Origine"]] == "REX-2021" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "royalblue3", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[
        roi_habitant[["Origine"]] == "PI-M2-ES-2020" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "khaki3", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[
        roi_habitant[["Origine"]] == "Stage-M2-ES-2020" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "palegreen2", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["Origine"]] == "PI-M2-ES-2019"],
    col = "black", bg = "khaki1", cex = 1.4,
    pch = 21, add = TRUE)

## Ajout de l'échelle
# raster::scalebar(10, type = "bar", divs = 4, below = "km")
terra::sbar(10, c(3.55, 43.47), type = "bar", below = "km", label = c(0, 5, 10), cex = .8)

## Ajout des crédits
# text(3.55, 43.435, credit, adj = 0, cex = .8, font = 3)

## Légende
# legend("topright", cex = .8, bg = "white", inset = 0.01,
    # title = "Land Use (from CLC)",
    # legend = c(
        # "Urban area",
        # "Agricultural area",
        # "Forest, natural area",
        # "Humid area",
        # "Water surface"),
    # fill = color_clc)

legend("bottomright", cex = .8, bg = "white", inset = 0.01,
    title = "Origine des candidats :",
    legend = c("REX 2021", "PI Master 2 ES 2020", "Stage Master 2 ES 2020", "PI Master 2 ES 2019"),
    pch = 21,
    col = "black",
    pt.bg = c("royalblue3", "khaki3", "palegreen2", "khaki1"),
    pt.cex = 1.4)

## Ajout de l'encart
# geau::add.inset(soii_france, col = c("white", "red"), border = c("lightgray", "red"), lwd = 2)

## Fermeture du fichier de sauvegarde
dev.off()  
```


```{r type_inondation}
# Production d'une carte

## Définition du nom du fichier de sauvegarde
file_path = file.path("data-common/figure/so-ii/pdf/2021-07-20/type_inondation.pdf")

## Récupération des données propre à la carte en cours
roi_habitant = rio::import("data-common/table/so-ii/roi-habitant-2021-07-20.ods", which = 1)
roi_habitant = roi_habitant[!is.na(roi_habitant$latitude), ]

## Transformation des données d'enquête en points géoréférencés
roi_habitant = sf::st_as_sf(roi_habitant, coords = c("longitude", "latitude"), crs = "WGS84")

# Génération de la carte

## Ouverture du fichier de sauvegarde
# png(file_path, height = 800, width = 800)
pdf(file_path)

## Initialisation de la carte avec le périmètre de so-ii
par(mai = c(.65, .60, .50, .15))
plot(soii, axes = TRUE)
# terra::plotRGB(soii_osm, add = TRUE)
plot(soii, lwd = 2, add = TRUE)

## Ajout du fond CLC avec le niveau 1 du code_18
color_clc = scales::alpha(c("red3", "darkolivegreen3", "darkgreen", "#4C90B4", "lightblue"), .2)
color = as.character(cut(
    as.integer(substr(soii_clc[["code_18"]], 1, 1)), 
    breaks = 5,
    labels = color_clc))
plot(soii_clc$geometry, border = NA, col = color, add = TRUE)

## Ajout des points d'enquête
# plot(roi_habitant$geometry, col = "red", bg = "red", pch = 21, cex = 1, add = TRUE)
plot(roi_habitant$geometry[
    roi_habitant[["type_alea"]] == "debordement-cours-eau" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "blue", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[
    roi_habitant[["type_alea"]] == "debordement-cours-eau_ruissellement" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "lightblue", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[
    roi_habitant[["type_alea"]] == "debordement-etang" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "hotpink1", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[
    roi_habitant[["type_alea"]] == "remontee-nappe" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "green", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[
    roi_habitant[["type_alea"]] == "ruissellement" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "brown", cex = 1.4,
    pch = 21, add = TRUE)    
plot(roi_habitant$geometry[
    roi_habitant[["type_alea"]] == "rupture-digue" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "orange", cex = 1.4,
    pch = 21, add = TRUE)

## Ajout de l'échelle
# raster::scalebar(10, type = "bar", divs = 4, below = "km")
terra::sbar(10, c(3.55, 43.47), type = "bar", below = "km", label = c(0, 5, 10), cex = .8)

## Ajout des crédits
# text(3.55, 43.435, credit, adj = 0, cex = .8, font = 3)

## Légende
# legend("topright", cex = .8, bg = "white", inset = 0.01,
    # title = "Land Use (from CLC)",
    # legend = c(
        # "Urban area",
        # "Agricultural area",
        # "Forest, natural area",
        # "Humid area",
        # "Water surface"),
    # fill = color_clc)

legend("bottomright", cex = .8, bg = "white", inset = 0.01,
    title = "Exposition principale des candidats :",
    legend = c("débordement de cours d'eau", "débordement et ruissellement", "débordement d'étang", "remontée de nappe", "ruissellement", "rupture de digue"),
    pch = 21,
    col = "black",
    pt.bg = c("blue", "lightblue", "hotpink1", "green", "brown", "orange"),
    pt.cex = 1.4)

## Ajout de l'encart
# geau::add.inset(soii_france, col = c("white", "red"), border = c("lightgray", "red"), lwd = 2)

## Fermeture du fichier de sauvegarde
dev.off()  
```


```{r annee_emmenagement}
# Production d'une carte

## Définition du nom du fichier de sauvegarde
file_path = file.path("data-common/figure/so-ii/pdf/2021-07-20/annee_emmenagement.pdf")

## Récupération des données propre à la carte en cours
roi_habitant = rio::import("data-common/table/so-ii/roi-habitant-2021-07-20.ods", which = 1)
roi_habitant = roi_habitant[!is.na(roi_habitant$latitude), ]

## Transformation des données d'enquête en points géoréférencés
roi_habitant = sf::st_as_sf(roi_habitant, coords = c("longitude", "latitude"), crs = "WGS84")

# Génération de la carte

## Ouverture du fichier de sauvegarde
# png(file_path, height = 800, width = 800)
pdf(file_path)

## Initialisation de la carte avec le périmètre de so-ii
par(mai = c(.65, .60, .50, .15))
plot(soii, axes = TRUE)
# terra::plotRGB(soii_osm, add = TRUE)
plot(soii, lwd = 2, add = TRUE)

## Ajout du fond CLC avec le niveau 1 du code_18
color_clc = scales::alpha(c("red3", "darkolivegreen3", "darkgreen", "#4C90B4", "lightblue"), .2)
color = as.character(cut(
    as.integer(substr(soii_clc[["code_18"]], 1, 1)), 
    breaks = 5,
    labels = color_clc))
plot(soii_clc$geometry, border = NA, col = color, add = TRUE)

## Ajout des points d'enquête
# plot(roi_habitant$geometry, col = "red", bg = "red", pch = 21, cex = 1, add = TRUE)
plot(roi_habitant$geometry[
        roi_habitant[["annee_emmenagement"]] >= 2000  &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "white", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[
        roi_habitant[["annee_emmenagement"]] < 2000 &
        roi_habitant[["annee_emmenagement"]] >= 1987 &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "lightcoral", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[ &
        roi_habitant[["annee_emmenagement"]] == 1970  &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "brown", cex = 1.4,
    pch = 21, add = TRUE)

## Ajout des crédits
# text(3.55, 43.435, credit, adj = 0, cex = .8, font = 3)

## Légende
# legend("topright", cex = .8, bg = "white", inset = 0.01,
    # title = "Land Use (from CLC)",
    # legend = c(
        # "Urban area",
        # "Agricultural area",
        # "Forest, natural area",
        # "Humid area",
        # "Water surface"),
    # fill = color_clc)

legend("bottomright", cex = .8, bg = "white", inset = 0.01,
    title = "Année d'emménagement des candidats :",
    legend = c("2000-2021", "1987-2000", "1970-1987", "Avant 1970"),
    pch = 21,
    col = "black",
    pt.bg = c("white", "lightcoral", "red", "brown"),
    pt.cex = 1.4)

## Ajout de l'encart
# geau::add.inset(soii_france, col = c("white", "red"), border = c("lightgray", "red"), lwd = 2)

## Fermeture du fichier de sauvegarde
dev.off()  
```

```{r age_candidats}
# Production d'une carte

## Définition du nom du fichier de sauvegarde
file_path = file.path("data-common/figure/so-ii/pdf/2021-07-20/age_candidats.pdf")

## Récupération des données propre à la carte en cours
roi_habitant = rio::import("data-common/table/so-ii/roi-habitant-2021-07-20.ods", which = 1)
roi_habitant = roi_habitant[!is.na(roi_habitant$latitude), ]

## Transformation des données d'enquête en points géoréférencés
roi_habitant = sf::st_as_sf(roi_habitant, coords = c("longitude", "latitude"), crs = "WGS84")

# Génération de la carte

## Ouverture du fichier de sauvegarde
# png(file_path, height = 800, width = 800)
pdf(file_path)

## Initialisation de la carte avec le périmètre de so-ii
par(mai = c(.65, .60, .50, .15))
plot(soii, axes = TRUE)
# terra::plotRGB(soii_osm, add = TRUE)
plot(soii, lwd = 2, add = TRUE)

## Ajout du fond CLC avec le niveau 1 du code_18
color_clc = scales::alpha(c("red3", "darkolivegreen3", "darkgreen", "#4C90B4", "lightblue"), .2)
color = as.character(cut(
    as.integer(substr(soii_clc[["code_18"]], 1, 1)), 
    breaks = 5,
    labels = color_clc))
plot(soii_clc$geometry, border = NA, col = color, add = TRUE)

## Ajout des points d'enquête
# plot(roi_habitant$geometry, col = "red", bg = "red", pch = 21, cex = 1, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["tranche_age"]] == "+71 ans" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "coral4", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["tranche_age"]] == "51-70" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "brown2", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["tranche_age"]] == "31-50" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "orange", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["tranche_age"]] == "18-30" &
        roi_habitant[["statut_personne"]] == "candidat"
        ],
    col = "black", bg = "gold", cex = 1.4,
    pch = 21, add = TRUE)

## Ajout de l'échelle
# raster::scalebar(10, type = "bar", divs = 4, below = "km")
terra::sbar(10, c(3.55, 43.47), type = "bar", below = "km", label = c(0, 5, 10), cex = .8)

## Ajout des crédits
# text(3.55, 43.435, credit, adj = 0, cex = .8, font = 3)

## Légende
# legend("topright", cex = .8, bg = "white", inset = 0.01,
    # title = "Land Use (from CLC)",
    # legend = c(
        # "Urban area",
        # "Agricultural area",
        # "Forest, natural area",
        # "Humid area",
        # "Water surface"),
    # fill = color_clc)

legend("bottomright", cex = .8, bg = "white", inset = 0.01,
    title = "Tranches d'âge des candidats :",
    legend = c("18-30", "31-50", "51-70", "+71 ans"),
    pch = 21,
    col = "black",
    pt.bg = c("gold", "orange", "brown2", "coral4"),
    pt.cex = 1.4)

## Ajout de l'encart
# geau::add.inset(soii_france, col = c("white", "red"), border = c("lightgray", "red"), lwd = 2)

## Fermeture du fichier de sauvegarde
dev.off()  
```


```{r ROI_agri}
# Production d'une carte

## Définition du nom du fichier de sauvegarde
file_path = file.path("data-common/figure/so-ii/pdf/2021-07-20/roi_agri.pdf")

## Récupération des données propre à la carte en cours
roi_habitant = rio::import("data-common/table/so-ii/roi-habitant-2021-07-20.ods", which = 1)
roi_habitant = roi_habitant[!is.na(roi_habitant$latitude), ]

## Transformation des données d'enquête en points géoréférencés
roi_habitant = sf::st_as_sf(roi_habitant, coords = c("longitude", "latitude"), crs = "WGS84")

# Génération de la carte

## Ouverture du fichier de sauvegarde
# png(file_path, height = 800, width = 800)
pdf(file_path)

## Initialisation de la carte avec le périmètre de so-ii
par(mai = c(.65, .60, .50, .15))
plot(soii, axes = TRUE)
# terra::plotRGB(soii_osm, add = TRUE)
plot(soii, lwd = 2, add = TRUE)

## Ajout du fond CLC avec le niveau 1 du code_18
color_clc = scales::alpha(c("red3", "darkolivegreen3", "darkgreen", "#4C90B4", "lightblue"), .2)
color = as.character(cut(
    as.integer(substr(soii_clc[["code_18"]], 1, 1)), 
    breaks = 5,
    labels = color_clc))
plot(soii_clc$geometry, border = NA, col = color, add = TRUE)

## Ajout des points d'enquête
# plot(roi_habitant$geometry, col = "red", bg = "red", pch = 21, cex = 1, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["tranche_age"]] == "+71 ans"],
    col = "black", bg = "brown", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["tranche_age"]] == "51-70"],
    col = "black", bg = "red", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["tranche_age"]] == "31-50"],
    col = "black", bg = "orange", cex = 1.4,
    pch = 21, add = TRUE)
plot(roi_habitant$geometry[roi_habitant[["tranche_age"]] == "18-30"],
    col = "black", bg = "lightyellow", cex = 1.4,
    pch = 21, add = TRUE)

## Ajout de l'échelle
# raster::scalebar(10, type = "bar", divs = 4, below = "km")
terra::sbar(10, c(3.55, 43.47), type = "bar", below = "km", label = c(0, 5, 10), cex = .8)

## Ajout des crédits
# text(3.55, 43.435, credit, adj = 0, cex = .8, font = 3)

## Légende
# legend("topright", cex = .8, bg = "white", inset = 0.01,
    # title = "Land Use (from CLC)",
    # legend = c(
        # "Urban area",
        # "Agricultural area",
        # "Forest, natural area",
        # "Humid area",
        # "Water surface"),
    # fill = color_clc)

legend("bottomright", cex = .8, bg = "white", inset = 0.01,
    title = "Tranches d'âge des candidats :",
    legend = c("18-30", "31-50", "51-70", "+71 ans"),
    pch = 21,
    col = "black",
    pt.bg = c("lightyellow", "orange", "red", "brown"),
    pt.cex = 1.4)

## Ajout de l'encart
# geau::add.inset(soii_france, col = c("white", "red"), border = c("lightgray", "red"), lwd = 2)

## Fermeture du fichier de sauvegarde
dev.off()  
```



```{r experts_assurance}
# Production d'une carte

## Définition du nom du fichier de sauvegarde
file_path = file.path("data-common/figure/so-ii/pdf/2021-07-20/experts_assurance.pdf")

## Récupération des données propre à la carte en cours
experts_assurance = rio::import("data-common/table/so-ii/experts-assurance-2021.ods", which = 1)
experts_assurance = experts_assurance[!is.na(experts_assurance$latitude), ]

## Transformation des données d'enquête en points géoréférencés
experts_assurance = sf::st_as_sf(experts_assurance, coords = c("longitude", "latitude"), crs = "WGS84")

# Génération de la carte

## Ouverture du fichier de sauvegarde
# png(file_path, height = 800, width = 800)
pdf(file_path)

## Initialisation de la carte avec le périmètre de la France
par(mai = c(.65, .60, .50, .15))
plot(soii_france, axes = TRUE)
# terra::plotRGB(soii_osm, add = TRUE)
plot(soii_france, lwd = 2, add = TRUE)

## Ajout des points des bureaux d'expertise

# plot(experts_assurance$geometry, col = "red", bg = "red", pch = 21, cex = 1, add = TRUE)
plot(experts_assurance$geometry[experts_assurance[["statut"]] == "Rencontré"],
    col = "black", bg = "green", cex = 1.4,
    pch = 21, add = TRUE)
plot(experts_assurance$geometry[experts_assurance[["statut"]] == "Contacté"],
    col = "black", bg = "yellow", cex = 1.4,
    pch = 21, add = TRUE)
plot(experts_assurance$geometry[experts_assurance[["statut"]] == "Prévu"],
    col = "black", bg = "white", cex = 1.4,
    pch = 21, add = TRUE)

## Ajout de l'échelle
# raster::scalebar(10, type = "bar", divs = 4, below = "km")
terra::sbar(10, c(3.55, 43.47), type = "bar", below = "km", label = c(0, 5, 10), cex = .8)

## Ajout des crédits
# text(3.55, 43.435, credit, adj = 0, cex = .8, font = 3)

## Légende

legend("bottomright", cex = .8, bg = "white", inset = 0.01,
    title = "Rencontre avec les experts d'assurance :",
    legend = c("Rencontré", "Contacté", "Prévu"),
    pch = 21,
    col = "black",
    pt.bg = c("green", "yellow", "white"),
    pt.cex = 1.4)

## Ajout de l'encart
# geau::add.inset(soii_france, col = c("white", "red"), border = c("lightgray", "red"), lwd = 2)

## Fermeture du fichier de sauvegarde
dev.off()  
```
